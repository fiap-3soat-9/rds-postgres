terraform {
  required_version = ">=1.7.4"
required_providers {
    aws = ">=5.41.0"
    local = ">=2.5.1"
  }

  backend "s3" {
    bucket = "3soat-9-rds"
    key = "terraform.tfstate"
    region = "us-east-1"
  }
}

provider "aws" {
  region = "us-east-1"
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
}