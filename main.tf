data "aws_vpc" "vpc" {
  filter {
    name   = "tag:Name"
    values = ["soat3gp9-vpc"]
  }
}

data "aws_security_group" "sg" {
  filter {
    name   = "tag:Name"
    values = ["soat3gp9-db-sg"]
  }
}

data "aws_subnet" "subnet0" {
  filter {
    name   = "tag:Name"
    values = ["soat3gp9-private-subnet-0"]
  }
}

data "aws_subnet" "subnet1" {
  filter {
    name   = "tag:Name"
    values = ["soat3gp9-private-subnet-1"]
  }
}


resource "aws_db_subnet_group" "db-subnet" {
  name = "rds-subnet-group"
  subnet_ids = [data.aws_subnet.subnet0.id, data.aws_subnet.subnet1.id]
}

resource "aws_db_parameter_group" "parameter-group" {
  name   = "no-ssl"
  family = "postgres16"

  parameter {
    name  = "rds.force_ssl"
    value = "0"
  }

}

resource "aws_db_instance" "hamburgueria" {
  identifier             = "hamburgueria"
  db_name                = "hamburgueria"
  instance_class         = "db.t3.micro"
  allocated_storage      = 5
  engine                 = "postgres"
  engine_version         = "16.1"
  skip_final_snapshot    = true
  publicly_accessible    = true
  vpc_security_group_ids = [data.aws_security_group.sg.id]
  db_subnet_group_name = aws_db_subnet_group.db-subnet.name
  username               = "hamburgueria"
  password               = var.database_secret
  parameter_group_name = aws_db_parameter_group.parameter-group.name
}