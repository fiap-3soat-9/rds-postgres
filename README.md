# rds-postgres

Projeto responsável por criar um banco postgres através de RDS utilizando terraform

Para executar, basta utilizar os seguintes comandos:

`note: Necessário já existir a VPC configurada com suas subnets e security group`
`note2: Necessário existir o bucket s3 - 3soat-9-rds para salvar o estado do terraform`

```
    terraform init
    terraform plan -out "planfile"
    terraform apply -input=false ${TF_ROOT}/planfile
```
